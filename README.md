# Complete Guide to CRUD ExpressJS MySQL


Code for the entire grafikart tutorial series: Node JS zero To Hero

## Instructions

If you would like to download the code and try it for yourself:

Clone the repo: https://harrylepap@bitbucket.org/harrylepap/crud-express-mysql.git
Install packages: npm install
Change out the database configuration in config/db.js
Launch: npm run start
Visit in your browser at: http://localhost:3000

