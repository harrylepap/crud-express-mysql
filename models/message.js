
let connection = require('../config/db');
let moment = require('../config/moment');


class Message{

    constructor (row){
        this.row = row;
    }
    
    get id(){
        return this.row.id
    }

    get content(){
        return this.row.content;
    }

    get created_at(){
        return moment(this.row.created_at);
    }

    static create(content, cb){
        connection.query('INSERT INTO message SET content = ?, created_at = ?', [content, new Date()], (erreur, resultats) => {
            if(erreur) throw erreur;
            cb(resultats);
        })
    }

    static all(cb){
        connection.query('SELECT * FROM message ORDER BY id DESC', (erreur, rows) => {
            if(erreur) throw erreur;
            cb(rows.map((row) => new Message(row)))
        });
    }

    static find(id, cb){
        connection.query('SELECT * FROM message WHERE id = ? LIMIT 1', [id] ,(erreur, rows) => {
            if(erreur) throw erreur;
            cb(new Message(rows[0]))
        });
    }
}

module.exports = Message;